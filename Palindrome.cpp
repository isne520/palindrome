#include <iostream>
#include <cstring>
using namespace std;
int main()
{
	char inpstr[100], revstr[100], stripstr[100];
	int i, j = 0; 
 
	//print about the program
	cout << "Palindrome Program in C" << endl;
	cout << "Enter a string: ";
    
    //get the input string from user
   	gets(inpstr);
 
	//remove spaces & special characters
	for(i=0; i < strlen(inpstr); i++)
	{
	    if (isalnum(inpstr[i]))
	    {
	        stripstr[j] = inpstr[i];
	        j++;
	    }
	}
	stripstr[j] = '\0';
	
	//reset i to 0
	i = 0;
	
	//convert all characters to lowercase
	while( stripstr[i] ) {
      //store back the lower letter to mystr
      stripstr[i] = tolower(stripstr[i]);
      i++;
	}    
	
	//copy stripstr to revstr 	
   	strcpy(revstr, stripstr);
	
	//now reverse the revstr
	strrev(revstr);
	
	//compare if both the string are equal
	//if equal it is palindrome or it is not simple isn't :)
	
   	if( strcmp(stripstr,revstr) == 0){
		cout << "It is Palindrome";
	}else{
		cout << "Not a Palindrome";
	}
	
	return 0;
}
